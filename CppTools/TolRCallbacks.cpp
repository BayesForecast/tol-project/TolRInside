#include "TolRCallbacks.h"
#include "tol/tol_bout.h"

void TolRCallbacks::WriteConsole( const std::string& line, int type )
{
  if ( type == 0 )
    {
    Std( line.c_str( ) );
    }
  else if ( type == 1 )
    {
    Error( line.c_str( ) );    
    }
  else
    {
    std::ostringstream str;
    
    str << "from R: type = " << type << " line = " << line << std::endl;
    Std( str.str( ).c_str( ) );
    }
}
