#define LOCAL_NAMEBLOCK local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bnameblock.h>
#include "TolRCallbacks.h"
#include "RInside.h"
#include <tol/tol_bdatgra.h>

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& LOCAL_NAMEBLOCK = _local_unb_->Contens();

static RInside *rInterpreter = NULL;

#if defined(RINSIDE_CALLBACKS)
static TolRCallbacks * rCallbacks = NULL;
#endif

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockTolREmbedded( )
{
  if (!rInterpreter )
    {
    rInterpreter = new RInside( 0, NULL );
#if defined(RINSIDE_CALLBACKS)
    rCallbacks = new TolRCallbacks( );
    rInterpreter->set_callbacks( rCallbacks );
#endif
    }
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = LOCAL_NAMEBLOCK;
  return( copy );
}

//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatEmbedded);
DefMethod(1, BDatEmbedded, "StatusEmbedded", 1, 1, "Real", "(Real void)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatEmbedded::CalcContens()
{
  contens_ = BDat( 1 );
}
