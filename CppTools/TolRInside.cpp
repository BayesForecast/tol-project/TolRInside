/* TolRInside.cpp: C++ API to invoke R from TOL

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bsetgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bdtegra.h>
#include "TolRConverters.h"
#include "TolR.h"
#include "Rinternals.h"

#define __TEST_DEVELOPMENT__

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& LOCAL_NAMEBLOCK = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockTolRInside()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

//---------------------------------------------------------------------------
DeclareContensClass(BText, BTxtTemporary, BTextRVersion);
DefMethod(1, BTextRVersion, "R.version.string", 1, 1, "Real", "(Real void)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BTextRVersion::CalcContens()
{
  try 
      {
      Rcpp::Environment baseEnv("package:base");
      std::string ver = baseEnv["R.version.string"];
      contens_.Copy( ver.c_str( ) );
      } 
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      }
}

//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatNewEnv);
DefMethod(1, BDatNewEnv, "NewEnv", 1, 1, "Text", "(Text name)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatNewEnv::CalcContens()
{
  try 
      {
      BText &name = Text( Arg( 1 ) );
      Rcpp::Environment env = Rcpp::new_env();
      //(*rInterpreter)[ name.String( ) ] = env;
      Rcpp::Environment glob_env = Rcpp::Environment::global_env( );
      glob_env.assign( name.String( ), env );
      contens_ = 1;
      }
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      }
}

//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatRemoveEnv);
DefMethod(1, BDatRemoveEnv, "RemoveEnv", 1, 1, "Text", "(Text name)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatRemoveEnv::CalcContens()
{
  try 
      {
      BText &name = Text( Arg( 1 ) );
      //SEXP x = (*rInterpreter)[ name.String( ) ];
      SEXP x = TolR::Access( name.String( ) );
      if( Rf_isEnvironment( x ) )
        {
        Rcpp::Environment glob_env = Rcpp::Environment::global_env( );
        contens_ = glob_env.remove( name.String( ) );
        }
      else
        {
        Error( BText( "" ) << name << " is not an environment" );
        contens_ = 0;
        }
      }
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      contens_ = 0;
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      contens_ = 0;
      }
}

//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatSetVar);
DefMethod(1, BDatSetVar, "SetVar", 2, 3, "Text Anything Text", "(Text name, Anything x [, Text env])",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatSetVar::CalcContens()
{
  try 
    {
    BText &vname = Text( Arg( 1 ) );
    Rcpp::Environment env;
    if ( NumArgs( ) > 2 )
      {
      BText &ename = Text( Arg( 3 ) );
      //SEXP _env = (*rInterpreter)[ ename.String( ) ];
      SEXP _env = TolR::Access( ename.String( ) );
      if ( Rf_isEnvironment( _env ) )
        {
        env = _env;
        }
      else
        {
        Error( BText( "" ) << ename << " is not an environment" );
        contens_ = 0;
        return;
        }
      } 
    else 
      {
      env = Rcpp::Environment::global_env( );
      }
    SEXP x = Rcpp::wrap( Arg( 2 ) );
    if ( x == R_NilValue )
      {
      Error( BText( "Could not convert " ) << Arg(2)->Grammar()->Name() << " to R" );
      contens_ = 0;
      }
    else
      {
      contens_ = env.assign( vname.String( ), x );
      }
    }
  catch(std::exception& ex) 
    {
    BText err( "Exception caught: " );
    err += ex.what();
    Error( err );
    contens_ = 0;
    } 
  catch(...) 
    {
    Error( "Unknown exception caught" );
    contens_ = 0;
    }
}

//---------------------------------------------------------------------------
DeclareContensClass(BSet, BSetTemporary, BDatGetVar);
DefMethod(1, BDatGetVar, "GetVar", 1, 2, "Text Text", "(Text name [, Text env])",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatGetVar::CalcContens()
{
  try 
    {
    BText &vname = Text( Arg( 1 ) );
    Rcpp::Environment env;
    if ( NumArgs( ) > 1 )
      {
      BText &ename = Text( Arg( 2 ) );
      //SEXP _env = (*rInterpreter)[ ename.String( ) ];
      SEXP _env = TolR::Access( ename.String( ) );
      if ( Rf_isEnvironment( _env ) )
        {
        env = _env;
        }
      else
        {
        Error( BText( "" ) << ename << " is not an environment" );
        return;
        }
      } 
    else 
      {
      env = Rcpp::Environment::global_env( );
      }
    SEXP rVar = env.get( vname.String( ) );
    if ( rVar == R_NilValue )
      {
      Error( BText( "Variable " ) << vname << " not found" );
      }
    else
      {
      BSyntaxObject *tolVar = Rcpp::as<BSyntaxObject*>( rVar );
      if ( tolVar )
        {
        tolVar->PutName( vname );
        contens_.RobElement( Cons( tolVar, NIL ) );
        }
      }
    }
  catch(std::exception& ex) 
    {
    BText err( "Exception caught: " );
    err += ex.what();
    Error( err );
    } 
  catch(...) 
    {
    Error( "Unknown exception caught" );
    }
}

#if defined( __TEST_DEVELOPMENT__ )

/*
//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatHelloWorld);
DefMethod(1, BDatHelloWorld, "HelloWorld", 1, 1, "Real", "(Real void)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatHelloWorld::CalcContens()
{
  //(*rInterpreter)["txt"] = "Hello, world!\n";       // assign a char* (string) to 'txt'
  TolR::Access( "txt_hello_world" ) = "Hello, world!\n"; 
  //rInterpreter->parseEvalQ("cat(txt)");           // eval the string, ignoring any returns
  TolR::ParseEvalQ( "cat(txt)" );
  contens_ = BDat( 1 );
}
*/

//---------------------------------------------------------------------------
DeclareContensClass(BDate, BDteTemporary, BDteRToday);
DefMethod(1, BDteRToday, "Today", 1, 1, "Real", "(Real void)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDteRToday::CalcContens()
{
  try 
      {
      //contens_ = rInterpreter->parseEval("Sys.Date()");
      contens_ = TolR::ParseEval( "Sys.Date()" );
      }
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      }
}

//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatAsReal);
DefMethod(1, BDatAsReal, "AsReal", 1, 1, "Text", "(Text RExpr)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatAsReal::CalcContens()
{
  try 
      {
      BText &rExpr = Text( Arg( 1 ) );
      // BDat r = rInterpreter->parseEval( rExpr.String( ) );
      BDat r = TolR::ParseEval( rExpr.String( ) );
      contens_ = BDat( r );
      }
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      }
}

//---------------------------------------------------------------------------
DeclareContensClass(BText, BTxtTemporary, BTxtAsText);
DefMethod(1, BTxtAsText, "AsText", 1, 1, "Text", "(Text RExpr)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BTxtAsText::CalcContens()
{
  try 
      {
      BText &rExpr = Text( Arg( 1 ) );
      // contens_ = Rcpp::as<BText>(rInterpreter->parseEval(rExpr.String( ) ) );
      contens_ = Rcpp::as<BText>( TolR::ParseEval(rExpr.String( ) ) );
      }
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      }
}

//---------------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatAsMatrix);
DefMethod(1, BMatAsMatrix, "AsMatrix", 1, 1, "Text", "(Text RExpr)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BMatAsMatrix::CalcContens()
{
  try 
      {
      BText &rExpr = Text( Arg( 1 ) );
      /*
      Rcpp::AsBMat( rInterpreter->parseEval( rExpr.String( ) ),
                    contens_ );
      */
      Rcpp::AsBMat( TolR::ParseEval( rExpr.String( ) ), contens_ );
      }
    catch(std::exception& ex) 
      {
      BText err( "Exception caught: " );
      err += ex.what();
      Error( err );
      } 
    catch(...) 
      {
      Error( "Unknown exception caught" );
      }
}

#endif
